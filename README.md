### What is the "Stretch-clock"?

The stretch clock is a timer. I know, it's so original.

### Why do we need a Stretch-Clock?

You don't really. You can keep timing your stretch routines like you always have.

### Then why build one?

I was playing basketball by myself one day and after my session of said hoops I decided it was probably a good idea to stretch because reasons. While I was doing this stretch routine I found that using the timer on my fitbit was kind of annoying, I had to keep checking it and keep track of how many rounds I completed which got in the way of me focusing on doing my stretch effectively. I took notice of the wonderful opportunity that was presenting itself and said "I'm going to build a web app, that I can preset the time for my stretches and the amount of rounds. It will give me cues to when a round has ended and when all my rounds are completed, a sort of fire and forget (but reminds you with cues) timer for stretching. Finally I can use these Web skills I've been learning... Huzzah!"

## 3-5-2018  Update

It's been a while since i've worked on this, but I've also learned a lot in that time. So hopefully I'll be able to take this further.

