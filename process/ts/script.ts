// Should replace window.onload with a closure, also look into using DOMContentLoaded
/* Recreate Timer as class, for reuse and for having many timers */
'use strict';

window.onload = function() {
  // I first want to check for indexedDB
  if(!('indexedDB' in window)) {
    console.warn('IndexedDB not supported');
    return;
  }
  
  // ...IndexedDB code
  const dbName = "my exercise";
  const version = 1;
  idb.open(dbName, version, (upgradeDb) => {
    upgradeDb.createObjectStore('store1')
  })
  .then(db => console.log('success'));


  // ...Timer code
  const timerComponent = document.querySelector(".timer"),
      setInputContainer = document.querySelector(".set-input"),
      timeInputContainer = document.querySelector(".time-input"),
      restContainer = document.querySelector(".rest-container"),
      titleDisplay = document.querySelector(".title-display"),
      stateDisplay = document.querySelector(".state-display"),
      setDisplay = document.querySelector(".sets-display"),
      timerDisplay = document.querySelector(".timer-display"),
      restDisplay = document.querySelector(".rest-display"),
      titleInput = document.querySelector("#title"),
      setInput = document.querySelector("#sets"),
      timerInput = document.querySelector("#time"),
      editButton = document.querySelector("#edit-btn"),
      playButton = document.querySelector("#start-btn"),
      stopButton = document.querySelector("#stop-btn"); // grab all the components from the site and assign to variables

  // I saw on the Naviant site that as you enter in info it automatically switches to the next form field, I'd like to incorperate that into the timer
  // plus the validation (below), but I want validation to happen as input happens, so people get feedback quickly.
  var regEx = new RegExp('/^-?\d+\.?\d*$/'); // Regular expression for form validation

  var timeCount:number, setCount:number, countDownID:number, restIntervalID:number, timerTitle:string // batch declare variables
  
  // default rest time is 5
  var restCount:number = 5;  

  /* Need to go through and replace all innerHTML with textContent, where applicable */

  stateDisplay.textContent = timerComponent.getAttribute("data-state");
  
  restDisplay.textContent = restCount.toString();
  
  timeCount = Number(timerInput.getAttribute("placeholder")); // initialize timeCount variable
  
  setCount = Number(setInput.getAttribute("placeholder")); // initialize setCount variable
  
  setDisplay.textContent = setCount.toString();
  
  timerDisplay.textContent = timeCount.toString();
  
  // event handler on editButton press
  editButton.addEventListener("click", editTimer, false); 

  // event handler on playButton press
  playButton.addEventListener("click", startTimer, false);

  // event handler on stopButton
  stopButton.addEventListener("click", pauseTimer, false); 

  function updateTimer() {
    setCount--;
    if(setCount >= 0) {
      setDisplay.textContent = setCount.toString();
      timeCount = Number(timerInput.getAttribute("placeholder"));
      restCount = 5;
      restDisplay.textContent = restCount.toString();
      timerDisplay.textContent = timerInput.getAttribute("placeholder");
      timerComponent.setAttribute("data-state", "ready");
      console.log("set count: " + setCount);
      startTimer();
    } else {
      console.log("you are done stretching");
      timerComponent.setAttribute("data-state", "finished");
      return;
    }
  } // function that updates the sets

  function startTimer() {
    
    // if the timerComponent data-state attribute is not "stretch" AND is not "editing"...
    if(timerComponent.getAttribute("data-state") != "stretching" && timerComponent.getAttribute("data-state") != "editing") {
    
      // start an interval of 1 second with the callback initTime. assign to countDownID, so I can clear the interval later;
      countDownID = setInterval(initTime, 1000);
    
      // update the state of the timer Component
      updateTimerState("stretching");
    
    } else { // Otherwise ...
      
      // If data-state is "editing"...
      if(timerComponent.getAttribute("data-state") == "editing") {
      
        // tell the user to finish editing the timer
        console.log("finish editing the timer");
      
      }
      
      // if data-state is "stretching"...
      if(timerComponent.getAttribute("data-state") == "stretching") {
      
        // tell the console the Timer has already started, probably should incorperate this into the UI for better UX. GP doesn't really know about the console.
        console.log("Timer has already started");
     
      }
    
    }
  } // function that initiates the timer

  function pauseTimer() {
    // check if the state is "stretching"...
    if(timerComponent.getAttribute("data-state") == "stretching") {
      
      // update the state
      updateTimerState("paused");
      
      // clear the coundDownID interval
      clearInterval(countDownID);
      
      // console.log for debugging
      console.log("Paused the timer");
    }
  
  } // function that will pause the timer

  function startRest() {
    // add "active" to classList
    restContainer.classList.add("active");
    
    // update the timer state
    updateTimerState("resting");
    
    // start a new interval, with the initRest callback => start the rest timer assign to variable so I can clear it later.
    restIntervalID = setInterval(initRest, 1000);

  } // function that starts the rest between sets

  function createWarning(parent, warningString) {
    if(parent.dataset.warningIssued) {
      var warning = document.createElement("span");
      var warningText = document.createTextNode(warningString);
      warning.setAttribute("class", "warning");
      warning.appendChild(warningText);
      parent.appendChild(warning);
      parent.dataset.warningIssued = true;
    }
  } // function that creates a warning element and gives it custom text box and appends it to the given parent

  function editTimer() {
    updateTimerState("editing"); // update the state of the timer
    console.log("initiate edit of timer"); // debug code to let me know where I am
    setInput.classList.toggle("active");
    timerInput.classList.toggle("active");
    setInput.focus();

    setInput.addEventListener("keyup", setSets, false); // event handler for set input
    timerInput.addEventListener("keyup", setTime, false); // event handler for time input
  } // editTimer

  function removeWarning (parent) {
    if(parent.dataset.warningIssuedtrue) {
      for (var i in parent.childNodes) {
        if(parent.childNodes[i].className == "warning") {
          parent.childNodes[i].remove();
        }
      }
      parent.dataset.warningIssued = false;
    }
  } // Function that checks for className "warning" and removes it

  function updateTimerState(state) {
    timerComponent.setAttribute("data-state", state);
    stateDisplay.textContent = timerComponent.getAttribute("data-state");
  } // updateTimerState

  function setSets(e) {
    if(e.keyCode === 13 && document.activeElement === setInput) {
      setCount = this.value;
      this.placeholder = setCount;
      console.log("setInput value: " + this.value);
      setDisplay.textContent = setCount.toString();
      this.value = '';
      this.classList.remove("active");
      timerInput.focus();
    }
  } // callback for setting Set Display

  function setTime(e) {
    console.log(e.keyCode);
    if(e.keyCode === 13 && document.activeElement === timerInput) {
      timeCount = this.value;
      this.placeholder = timeCount;
      console.log("timerInput value:" + this.value);
      timerDisplay.textContent = timeCount.toString();
      this.value = '';
      this.classList.remove("active");
      updateTimerState("set");
    }
  } // callback for setting the Time Display

  function initTime() {
    timeCount--;
    timerDisplay.textContent = timeCount.toString();
    if(timeCount == 0) {
      console.log(`time: ${timeCount}`);  
      pauseTimer();
      startRest();
    }
  } // initTime

  function initRest() {
    // Decrement the restCount
    restCount--;
    // set the restDisplay to the restCount as a string, Should replace innerHTML with textContent
    restDisplay.textContent = restCount.toString();
    // if restCount equals 0...
    if(restCount == 0) {
      // clear the rest interval
      clearInterval(restIntervalID);

      // remove the class "active" from the classList
      restContainer.classList.remove("active");

      // invoke updateTimer
      updateTimer();
    }
  } // initRest

} // window.onload
