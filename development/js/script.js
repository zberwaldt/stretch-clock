'use strict';
window.onload = function () {
    if (!('indexedDB' in window)) {
        console.warn('IndexedDB not supported');
        return;
    }
    var dbName = "my exercise";
    var version = 1;
    idb.open(dbName, version, function (upgradeDb) {
        upgradeDb.createObjectStore('store1');
    })
        .then(function (db) { return console.log('success'); });
    var timerComponent = document.querySelector(".timer"), setInputContainer = document.querySelector(".set-input"), timeInputContainer = document.querySelector(".time-input"), restContainer = document.querySelector(".rest-container"), titleDisplay = document.querySelector(".title-display"), stateDisplay = document.querySelector(".state-display"), setDisplay = document.querySelector(".sets-display"), timerDisplay = document.querySelector(".timer-display"), restDisplay = document.querySelector(".rest-display"), titleInput = document.querySelector("#title"), setInput = document.querySelector("#sets"), timerInput = document.querySelector("#time"), editButton = document.querySelector("#edit-btn"), playButton = document.querySelector("#start-btn"), stopButton = document.querySelector("#stop-btn");
    var regEx = new RegExp('/^-?\d+\.?\d*$/');
    var timeCount, setCount, countDownID, restIntervalID, timerTitle;
    var restCount = 5;
    stateDisplay.textContent = timerComponent.getAttribute("data-state");
    restDisplay.textContent = restCount.toString();
    timeCount = Number(timerInput.getAttribute("placeholder"));
    setCount = Number(setInput.getAttribute("placeholder"));
    setDisplay.textContent = setCount.toString();
    timerDisplay.textContent = timeCount.toString();
    editButton.addEventListener("click", editTimer, false);
    playButton.addEventListener("click", startTimer, false);
    stopButton.addEventListener("click", pauseTimer, false);
    function updateTimer() {
        setCount--;
        if (setCount >= 0) {
            setDisplay.textContent = setCount.toString();
            timeCount = Number(timerInput.getAttribute("placeholder"));
            restCount = 5;
            restDisplay.textContent = restCount.toString();
            timerDisplay.textContent = timerInput.getAttribute("placeholder");
            timerComponent.setAttribute("data-state", "ready");
            console.log("set count: " + setCount);
            startTimer();
        }
        else {
            console.log("you are done stretching");
            timerComponent.setAttribute("data-state", "finished");
            return;
        }
    }
    function startTimer() {
        if (timerComponent.getAttribute("data-state") != "stretching" && timerComponent.getAttribute("data-state") != "editing") {
            countDownID = setInterval(initTime, 1000);
            updateTimerState("stretching");
        }
        else {
            if (timerComponent.getAttribute("data-state") == "editing") {
                console.log("finish editing the timer");
            }
            if (timerComponent.getAttribute("data-state") == "stretching") {
                console.log("Timer has already started");
            }
        }
    }
    function pauseTimer() {
        if (timerComponent.getAttribute("data-state") == "stretching") {
            updateTimerState("paused");
            clearInterval(countDownID);
            console.log("Paused the timer");
        }
    }
    function startRest() {
        restContainer.classList.add("active");
        updateTimerState("resting");
        restIntervalID = setInterval(initRest, 1000);
    }
    function createWarning(parent, warningString) {
        if (parent.dataset.warningIssued) {
            var warning = document.createElement("span");
            var warningText = document.createTextNode(warningString);
            warning.setAttribute("class", "warning");
            warning.appendChild(warningText);
            parent.appendChild(warning);
            parent.dataset.warningIssued = true;
        }
    }
    function editTimer() {
        updateTimerState("editing");
        console.log("initiate edit of timer");
        setInput.classList.toggle("active");
        timerInput.classList.toggle("active");
        setInput.focus();
        setInput.addEventListener("keyup", setSets, false);
        timerInput.addEventListener("keyup", setTime, false);
    }
    function removeWarning(parent) {
        if (parent.dataset.warningIssuedtrue) {
            for (var i in parent.childNodes) {
                if (parent.childNodes[i].className == "warning") {
                    parent.childNodes[i].remove();
                }
            }
            parent.dataset.warningIssued = false;
        }
    }
    function updateTimerState(state) {
        timerComponent.setAttribute("data-state", state);
        stateDisplay.textContent = timerComponent.getAttribute("data-state");
    }
    function setSets(e) {
        if (e.keyCode === 13 && document.activeElement === setInput) {
            setCount = this.value;
            this.placeholder = setCount;
            console.log("setInput value: " + this.value);
            setDisplay.textContent = setCount.toString();
            this.value = '';
            this.classList.remove("active");
            timerInput.focus();
        }
    }
    function setTime(e) {
        console.log(e.keyCode);
        if (e.keyCode === 13 && document.activeElement === timerInput) {
            timeCount = this.value;
            this.placeholder = timeCount;
            console.log("timerInput value:" + this.value);
            timerDisplay.textContent = timeCount.toString();
            this.value = '';
            this.classList.remove("active");
            updateTimerState("set");
        }
    }
    function initTime() {
        timeCount--;
        timerDisplay.textContent = timeCount.toString();
        if (timeCount == 0) {
            console.log("time: " + timeCount);
            pauseTimer();
            startRest();
        }
    }
    function initRest() {
        restCount--;
        restDisplay.textContent = restCount.toString();
        if (restCount == 0) {
            clearInterval(restIntervalID);
            restContainer.classList.remove("active");
            updateTimer();
        }
    }
};
//# sourceMappingURL=script.js.map